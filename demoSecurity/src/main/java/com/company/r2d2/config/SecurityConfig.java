/*
 * Copyright (c) 2021 R2D2
 */
package com.company.r2d2.config;

import com.company.r2d2.business.UserBusiness;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * Description:
 * 
 * @author arturoastorga file: SecurityConfig.java
 * @since Jul 18, 2021
 */
@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

  @Autowired
  private UserBusiness usuarioBusiness;
  
  @Autowired
  private BCryptPasswordEncoder bCryptPasswordEncoder;
  
  @Bean
  public BCryptPasswordEncoder passwordEncoder() {
    return new BCryptPasswordEncoder();
  }

  @Override
  protected void configure(AuthenticationManagerBuilder auth) throws Exception {
    //auth.inMemoryAuthentication().withUser("user").password("clave").roles("USER").and()
    //    .withUser("admin").password("password").roles("USER", "ADMIN");
    
    auth.userDetailsService(usuarioBusiness).passwordEncoder(bCryptPasswordEncoder);

  }
  protected void configuire(HttpSecurity httpSecurity) throws Exception {
    httpSecurity.authorizeRequests().anyRequest().authenticated().and().httpBasic();

  }
}

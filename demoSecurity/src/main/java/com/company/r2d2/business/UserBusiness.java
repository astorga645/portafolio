package com.company.r2d2.business;

import com.company.r2d2.entity.UsuarioEntity;
import com.company.r2d2.repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

@Service
public class UserBusiness implements UserDetailsService {
  
  @Autowired
  private UsuarioRepository usuarioRepository;
  
  @Override
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    UsuarioEntity usuarioEntity = usuarioRepository.findByNombre(username);
    
    List<GrantedAuthority> roles = new ArrayList<>();
    roles.add(new SimpleGrantedAuthority("ADMIN"));
    
    UserDetails userDetails = new User(usuarioEntity.getNombre(),usuarioEntity.getClave(),roles);
    return userDetails;
  }

}

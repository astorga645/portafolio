/*
 * Copyright (c) 2021 R2D2
 */
package com.company.r2d2.repository;

import com.company.r2d2.entity.PersonaEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Description: 
 * @author arturoastorga file: PersonaRepository.java
 * @since Jul 18, 2021
 */
public interface PersonaRepository extends JpaRepository<PersonaEntity, Integer> {

}

/*
 * Copyright (c) 2021 R2D2
 */
package com.company.r2d2.repository;

import com.company.r2d2.entity.UsuarioEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

/**
 * Description: 
 * @author arturoastorga file: UsuarioRepository.java
 * @since Jul 18, 2021
 */
@Component
public interface UsuarioRepository extends JpaRepository<UsuarioEntity, Integer> {

  UsuarioEntity findByNombre(String nombre);
}

/*
 * Copyright (c) 2021 R2D2
 */
package com.company.r2d2.entity;

import com.company.r2d2.constant.Constant;
import org.springframework.stereotype.Component;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Description: 
 * @author arturoastorga file: UsuarioEntity.java
 * @since Jul 18, 2021
 */
@Entity
@Component
@Table(name=Constant.NOMBRE_TABLA_USUARIO)
public class UsuarioEntity {
  
  @Id
  private int id;
  
  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getNombre() {
    return nombre;
  }

  public void setNombre(String nombre) {
    this.nombre = nombre;
  }

  public String getClave() {
    return clave;
  }

  public void setClave(String clave) {
    this.clave = clave;
  }

  private String nombre;
  
  private String clave;

}

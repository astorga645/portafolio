/*
 * Copyright (c) 2021 R2D2
 */
package com.company.r2d2.constant;

import org.springframework.stereotype.Component;

/**
 * Description: 
 * @author arturoastorga file: Constant.java
 * @since Jul 18, 2021
 */
@Component
public class Constant {
  
public static final String  ID_COLUMN = "idpersona";
  
  public static final String  NOMBRE_COLUMN = "nombre";
  
  public static final String  ID_DEFAULT = "1";
  
  public static final String  NOMBRE_DEFAULT = "Arturo";
  
  public static final String  NOMBRE_TABLA_PERSONA = "persona";
  
  public static final String  NOMBRE_TABLA_USUARIO = "usuario";
  
  public static final String  PATH_SALUDO = "/saludoName";
  
  public static final String  PATH_REGISTRO = "/registro";
  
  public static final String  PATH_LISTADO = "/listar";
  
  public static final String  PATH_REST = "/rest";
  
  public static final String  PARAMETRO_NAME = "name";
  
  public static final String  PARAMETRO_ID_PERSONA = "idPersona";
  
  public static final String SALUDO_NAME_HTML="saludoName";
  
  public static final String REGISTRO_HTML="registro";
  
  public static final String LISTAR_HTML="Listar";
  
  public static final String NAME_REPONSE_REPOSITORY="personas";
  
  public static final String VALUE_DELETE="/{id}";
  
  public static final String MSG_DELETE="Eliminacion de un registro.";
  
  public static final String MSG_SALUDO="Saludo de spring boot.";
  
  public static final String MSG_REGISTRO="Registro en base de datos.";
  
  public static final String MSG_LISTAR="Se solicito un listado.";
  
  public static final String MSG_REPONSE="Se accedio a la base de datos.";
  
  public static final String MSG_UPDATE="Actualizacion de un registro.";

}

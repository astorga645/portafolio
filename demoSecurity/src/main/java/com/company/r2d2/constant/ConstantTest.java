/*
 * Copyright (c) 2021 R2D2
 */
package com.company.r2d2.constant;

/**
 * Description: 
 * @author arturoastorga file: ConstantTest.java
 * @since Jul 18, 2021
 */
public class ConstantTest {
  
  public static final String CLAVE_TEST="arturoastorga";
  
  public static final String NOMBRE_TEST="jorge";
 
  public static final int ID_TEST=2;

}

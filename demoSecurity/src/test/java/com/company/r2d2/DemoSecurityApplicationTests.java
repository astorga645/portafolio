package com.company.r2d2;

import static org.junit.jupiter.api.Assertions.assertTrue;
import com.company.r2d2.constant.ConstantTest;
import com.company.r2d2.entity.UsuarioEntity;
import com.company.r2d2.repository.UsuarioRepository;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
class DemoSecurityApplicationTests {

  @Autowired
  private UsuarioRepository usuarioRepository;
  
  @Autowired
  private BCryptPasswordEncoder bCryptPasswordEncoder;

  @Test
  void contextLoads() {}

  @Test
  void crearUsuarioTest() {
    UsuarioEntity usuarioEntity = new UsuarioEntity();
    usuarioEntity.setClave(bCryptPasswordEncoder.encode(ConstantTest.CLAVE_TEST));
    usuarioEntity.setNombre(ConstantTest.NOMBRE_TEST);
    usuarioEntity.setId(ConstantTest.ID_TEST);
    
    UsuarioEntity responseEntity = usuarioRepository.save(usuarioEntity);
    
    assertTrue(responseEntity.getClave().equalsIgnoreCase(usuarioEntity.getClave()));
    
  }

}

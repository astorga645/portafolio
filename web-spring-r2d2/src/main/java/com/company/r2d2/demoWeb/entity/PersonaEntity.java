/*
 * Copyright (c) 2021 R2D2
 */
package com.company.r2d2.demoWeb.entity;

import com.company.r2d2.demoWeb.constant.Constant;
import org.springframework.stereotype.Component;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Description: 
 * @author arturoastorga file: PersonaEtity.java
 * @since Jul 17, 2021
 */
@Entity
@Component
@Table(name=Constant.NOMBRE_TABLA_PERSONA)
public class PersonaEntity {
  

  @Id
  @Column(name=Constant.ID_COLUMN)
  private int idPersona;
  
  @Column(name=Constant.NOMBRE_COLUMN, length=50 )
  private String nombre;

  public int getIdPersona() {
    return idPersona;
  }

  public void setIdPersona(int idPersona) {
    this.idPersona = idPersona;
  }

  public String getNombre() {
    return nombre;
  }

  public void setNombre(String nombre) {
    this.nombre = nombre;
  }
  
  

}

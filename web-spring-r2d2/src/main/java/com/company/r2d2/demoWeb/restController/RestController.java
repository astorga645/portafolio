/*
 * Copyright (c) 2021 R2D2
 */
package com.company.r2d2.demoWeb.restController;

import com.company.r2d2.demoWeb.constant.Constant;
import com.company.r2d2.demoWeb.entity.PersonaEntity;
import com.company.r2d2.demoWeb.repository.PersonaRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.List;

/**
 * Description: 
 * @author arturoastorga file: RestController.java
 * @since Jul 18, 2021
 */
@org.springframework.web.bind.annotation.RestController
@RequestMapping(Constant.PATH_REST)
public class RestController {
  
  private static Logger log = LoggerFactory.getLogger(RestController.class);
  
  @Autowired
  private PersonaRepository personaRepository;
  
  @GetMapping
  public List<PersonaEntity> listar(){
    log.debug(Constant.MSG_LISTAR);
    log.warn(Constant.MSG_REPONSE);
    return personaRepository.findAll();
    
  }
  
  @PostMapping
  public void registrar(@RequestBody PersonaEntity personaEntity){
    log.debug(Constant.MSG_REGISTRO);
    log.warn(Constant.MSG_REPONSE);
    personaRepository.save(personaEntity);
    
  }
  
  @PutMapping
  public void modificar(@RequestBody PersonaEntity personaEntity){
    log.debug(Constant.MSG_UPDATE);
    log.warn(Constant.MSG_REPONSE);
    personaRepository.save(personaEntity);
    
  }
  
  @DeleteMapping(value=Constant.VALUE_DELETE)
  public void eliminar(@PathVariable(Constant.PARAMETRO_ID_PERSONA) Integer idPErsona){
    log.debug(Constant.MSG_DELETE);
    log.warn(Constant.MSG_REPONSE);
    personaRepository.deleteById(idPErsona);
    
  }

}

/*
 * Copyright (c) 2021 R2D2
 */
package com.company.r2d2.demoWeb.controller;

import com.company.r2d2.demoWeb.constant.Constant;
import com.company.r2d2.demoWeb.entity.PersonaEntity;
import com.company.r2d2.demoWeb.repository.PersonaRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Description:
 * 
 * @author arturoastorga file: DemoController.java
 * @since Jul 16, 2021
 */
@Controller
public class DemoController {

  private static Logger log = LoggerFactory.getLogger(DemoController.class);
  @Autowired
  private PersonaRepository personaRepository;

  @Autowired
  private PersonaEntity personaEntity;

  @GetMapping(Constant.PATH_INDEX)
  public String index(Model model) {
    log.info(Constant.MSG_SALUDO);
    return Constant.INDEX_HTML;
  }
  
  @GetMapping(Constant.PATH_SALUDO)
  public String greeting(@RequestParam(name = Constant.PARAMETRO_NAME, required = false,
      defaultValue = Constant.NOMBRE_DEFAULT) String name, Model model) {
    model.addAttribute(Constant.PARAMETRO_NAME, name);
    log.info(Constant.MSG_SALUDO);
    return Constant.SALUDO_NAME_HTML;
  }

  @GetMapping(Constant.PATH_REGISTRO)
  public String registroPersona(
      @RequestParam(name = Constant.PARAMETRO_NAME, required = false,
          defaultValue = Constant.NOMBRE_DEFAULT) String name,
      @RequestParam(name = Constant.PARAMETRO_ID_PERSONA, required = false,
          defaultValue = Constant.ID_DEFAULT) int idPersona,
      Model model) {
    model.addAttribute(Constant.PARAMETRO_NAME, name);
    model.addAttribute(Constant.PARAMETRO_ID_PERSONA, idPersona);
    personaEntity.setIdPersona(idPersona);
    personaEntity.setNombre(name);
    personaRepository.save(personaEntity);
    log.info(Constant.MSG_REGISTRO);
    log.info(Constant.MSG_REPONSE);
    return Constant.REGISTRO_HTML;
  }

  @GetMapping(Constant.PATH_LISTADO)
  public String list(Model model) {
    
    model.addAttribute(Constant.NAME_REPONSE_REPOSITORY, personaRepository.findAll());
    log.info(Constant.MSG_LISTAR);
    log.info(Constant.MSG_REPONSE);
    return Constant.LISTAR_HTML;
  }
}

package com.company.r2d2.demoWeb.repository;

import com.company.r2d2.demoWeb.entity.PersonaEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PersonaRepository extends JpaRepository<PersonaEntity, Integer> {

}

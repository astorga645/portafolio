CREATE DATABASE rinkurh
    WITH 
    OWNER = postgres
    ENCODING = 'UTF8'
    LC_COLLATE = 'C'
    LC_CTYPE = 'C'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;

CREATE TABLE employee(
	numeroEmpleado char(8) NOT NULL PRIMARY KEY,
	nombreEmpleado char(50) NOT NULL,
	rol char(2) NOT NULL,
	tipo char(2) NOT NULL,
	activo char(1) DEFAULT '1',
	fechaInsert timestamp without time zone
    DEFAULT CURRENT_TIMESTAMP
)


CREATE TABLE activity(
	numeroEmpleado char(8) not null,
	fechaRegistro date not null,
	entregas int not null default 0,
	turnoCubierto char(2) NOT NULL,
	fechaInsert timestamp without time zone
    DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY (numeroEmpleado,fechaRegistro)
)    
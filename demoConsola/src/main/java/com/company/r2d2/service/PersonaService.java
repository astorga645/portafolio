/*
 * Copyright (c) 2021 R2D2
 */
package com.company.r2d2.service;

/**
 * Description: 
 * @author arturoastorga file: PersonaService.java
 * @since Jul 16, 2021
 */
public interface PersonaService {

  
  /**
   * 
   * @param nombre
   */
  public void registro(String nombre);
}

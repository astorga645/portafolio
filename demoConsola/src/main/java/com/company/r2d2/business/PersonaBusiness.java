/*
 * Copyright (c) 2021 R2D2
 */
package com.company.r2d2.business;

import com.company.r2d2.constant.Constant;
import com.company.r2d2.repository.PersonaRepository;
import com.company.r2d2.service.PersonaService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Description: 
 * @author arturoastorga file: PersonaBusiness.java
 * @since Jul 16, 2021
 */
@Service
public class PersonaBusiness implements PersonaService {
  
  private static Logger log = LoggerFactory.getLogger(PersonaBusiness.class);
      
  @Autowired
  private PersonaRepository personaRepository;
  
  @Override
  public void registro(String nombre) {
    // TODO Auto-generated method stub
    log.warn(Constant.MENSSGE_BUSINESS);
    personaRepository.registrar(nombre);
    
  }

}

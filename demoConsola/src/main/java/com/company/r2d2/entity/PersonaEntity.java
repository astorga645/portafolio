/*
 * Copyright (c) 2021 R2D2
 */
package com.company.r2d2.entity;

/**
 * Description: 
 * @author arturoastorga file: PersonaEntity.java
 * @since Jul 16, 2021
 */
public interface PersonaEntity {
  /**
   * 
   * @param nombre
   */
  public void registrar( String nombre) ;
}

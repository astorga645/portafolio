package com.company.r2d2;

import com.company.r2d2.constant.Constant;
import com.company.r2d2.service.PersonaService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoConsolaApplication implements CommandLineRunner {

  private static Logger log = LoggerFactory.getLogger(DemoConsolaApplication.class);
  
  @Autowired
  private PersonaService personaService;

  public static void main(String[] args) {
    SpringApplication.run(DemoConsolaApplication.class, args);
  }

  @Override
  public void run(String... args) throws Exception {
    // TODO Auto-generated method stub
    log.info(Constant.MENSSGE);
    log.warn(Constant.WARNING);
    personaService.registro(Constant.MESSAGE_NAME);
  }

}

/*
 * Copyright (c) 2021 R2D2
 */
package com.company.r2d2.repository;

import com.company.r2d2.constant.Constant;
import com.company.r2d2.entity.PersonaEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

/**
 * Description: 
 * @author arturoastorga file: PersonaRepository.java
 * @since Jul 16, 2021
 */
@Repository
public class PersonaRepository implements PersonaEntity {
  
  private static Logger log = LoggerFactory.getLogger(PersonaRepository.class);
      
  @Override
  public void registrar(String nombre) {
    // TODO Auto-generated method stub
    log.info(nombre);
    log.warn(Constant.MENSSGE_REPOSITORY);
  }

}

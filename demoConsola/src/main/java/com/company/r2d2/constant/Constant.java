/*
 * Copyright (c) 2021 R2D2
 */
package com.company.r2d2.constant;

/**
 * Description:
 * 
 * @author arturoastorga file: Constant.java
 * @since Jul 16, 2021
 */
public class Constant {

  public static String MENSSGE = "Saludos desde una app en consola, con spring boot";

  public static String WARNING = "Aviso desde una app en consola, con spring boot";

  public static String MENSSGE_REPOSITORY = "Saludos desde la capa repository, con spring boot";

  public static String MENSSGE_BUSINESS = "Saludos desde la capa negocio, con spring boot";

  public static String MESSAGE_NAME = "ARTURO";

}
